;(function($, window, document, undefined){
/**
* @AUTHOR Francesco Delacqua
* sets all items in a container to the height of the tallest
*/

var pluginName = "sameHeight",
	defaults = {
		targetSelector: '.resizable'
	};

function Plugin(element, options) {
	this.element = element;
	this.options = $.extend( {}, defaults, options);
	this._defaults = defaults;

	this.init();
}

Plugin.prototype.init = function() {
	var self = this;

	self.resize();

	self.bindEvents();
};

Plugin.prototype.reset = function() {
	var selector = this.options.targetSelector;

	$(this.element).find(selector).outerHeight('auto');
};

Plugin.prototype.resize = function() {
	var selector = this.options.targetSelector,
		height = 0;

	$(this.element).find(selector).each(function(index,element){
		if($(element).outerHeight() > height) height = $(element).outerHeight();
	});

	$(this.element).find(selector).outerHeight(height);
};


Plugin.prototype.bindEvents = function() {
	var self = this;

	$(window).on('resize', function(e){
		self.reset();
		self.resize();
	});

};

	
$.fn[pluginName] = function(options){
	return this.each(function(){
		if ( !$.data(this, "plugin_" + pluginName )) {
			$.data( this, "plugin_" + pluginName,
			new Plugin( this, options )); //salvo un riferimento alla plugin nell'elemento
		}
	});
};

}( jQuery, window, document ));