;(function($, window, document, undefined){
/**
* @AUTHOR Francesco Delacqua
* sets the active class of a menu item accordingly to the page
*/

var pluginName = "setActiveMenu",
	defaults = {
		targetSelector: 'li',
		activeClass: 'active',
		RE: false
	};

function Plugin(element, options) {
	this.element = element;
	this.options = $.extend( {}, defaults, options);
	this._defaults = defaults;

	this.init();
}

Plugin.prototype.init = function() {
	var self = this;

	self.setActive();

	self.bindEvents();
};

Plugin.prototype.setActive = function() {
	var self = this,
		selector = self.options.targetSelector,
		protocol = window.location.protocol,
		host = window.location.host,
		path = window.location.pathname;

		$(this.element).find(selector).removeClass(self.options.activeClass);

		$(this.element).find(selector).each(function(index,element){
			var RE = self.options.RE || new RegExp('^'+protocol+'//'+host,'ig'),
				href = $(this).find('a').attr('href');

			href = href.replace(RE, '');

			if(href == path) $(this).addClass(self.options.activeClass);
		});
};


Plugin.prototype.bindEvents = function() {
	var self = this;
};

	
$.fn[pluginName] = function(options){
	return this.each(function(){
		if ( !$.data(this, "plugin_" + pluginName )) {
			$.data( this, "plugin_" + pluginName,
			new Plugin( this, options )); //salvo un riferimento alla plugin nell'elemento
		}
	});
};

}( jQuery, window, document ));